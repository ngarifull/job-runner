package com.ngariful.model;

public interface SimpleJob {
    void run();
    boolean supports(String code);
}
