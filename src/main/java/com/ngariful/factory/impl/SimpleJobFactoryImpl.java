package com.ngariful.factory.impl;

import com.ngariful.exception.JobNotFoundException;
import com.ngariful.factory.SimpleJobFactory;
import com.ngariful.model.SimpleJob;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class SimpleJobFactoryImpl implements SimpleJobFactory {
    private final List<SimpleJob> simpleJobList;

    @Override
    public SimpleJob apply(String code) {
        return simpleJobList.stream()
                .filter(simpleJob -> simpleJob.supports(code))
                .findFirst()
                .orElseThrow(JobNotFoundException.lazyCreate("Job with code : [%] not found", code));
    }
}
