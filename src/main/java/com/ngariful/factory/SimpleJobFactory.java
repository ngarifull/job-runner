package com.ngariful.factory;

import com.ngariful.model.SimpleJob;
import java.util.function.Function;

public interface SimpleJobFactory extends Function<String, SimpleJob> {
}
