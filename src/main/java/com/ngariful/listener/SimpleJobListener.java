package com.ngariful.listener;

import com.ngariful.service.SimpleJobRunner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;


@Slf4j
@Service
@RequiredArgsConstructor
public class SimpleJobListener {
    private final SimpleJobRunner simpleJobRunner;

    @RabbitListener(queues = "${rabbit.queue}")
    public void handler(String jobCode) {
        simpleJobRunner.start(jobCode);
    }
}
