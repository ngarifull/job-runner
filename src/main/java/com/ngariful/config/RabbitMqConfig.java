package com.ngariful.config;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Slf4j
@EnableConfigurationProperties
@Configuration
@NoArgsConstructor
public class RabbitMqConfig {
    @Value("${rabbit.queue}")
    public String QUEUE_NAME;

    @Value("${rabbit.exchange}")
    public String EXCHANGE_NAME;

    @Value("${rabbit.routing.key}")
    public String ROUTING_KEY;

    @Bean
    Queue createQueue() {
        log.info(String.format("Creating queue : %s", QUEUE_NAME));
        return new Queue(QUEUE_NAME, true, false, false);
    }

    @Bean
    DirectExchange exchange() {
        log.info(String.format("Creating exchange : %s", EXCHANGE_NAME));
        return new DirectExchange(EXCHANGE_NAME);
    }

    @Bean
    RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        log.info(String.format("Creating rabbit template : %s", connectionFactory.toString()));
        return new RabbitTemplate(connectionFactory);
    }

    @Bean
    Binding binding(Queue q, DirectExchange exchange) {
        log.info(String.format("bindin exchange with routing key : %s", ROUTING_KEY));
        return BindingBuilder.bind(q).to(exchange).with(ROUTING_KEY);
    }

    @Bean
    RabbitAdmin rabbitAdmin(ConnectionFactory connectionFactory, DirectExchange directExchange, Queue queue, Binding binding) {
        RabbitAdmin admin = new RabbitAdmin(connectionFactory);
        admin.setAutoStartup(true);
        admin.declareExchange(directExchange);
        admin.declareQueue(queue);
        admin.declareBinding(binding);

        log.info(String.format("creating rabbit exchange, queue, binding : %s %s %s",
                directExchange.getName(), queue.getName(), binding.toString()));
        log.info(String.format("connection factory is : %s", connectionFactory.toString()));
        return admin;
    }
}
