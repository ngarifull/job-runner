package com.ngariful.exception;

import java.util.function.Supplier;

public class JobNotFoundException extends RuntimeException {
    public JobNotFoundException(String message) {
        super(message);
    }

    public static JobNotFoundException create(String s, Object... args) {
        return new JobNotFoundException(String.format(s, args));
    }

    public static Supplier<? extends JobNotFoundException> lazyCreate(String message, Object... args) {
        return () -> create(message, args);
    }
}
