package com.ngariful.service.impl;

import com.ngariful.factory.SimpleJobFactory;
import com.ngariful.model.SimpleJob;
import com.ngariful.service.SimpleJobRunner;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SimpleJobRunnerImpl implements SimpleJobRunner {
    private final SimpleJobFactory simpleJobFactory;

    @Override
    public void start(String jobCode) {
        SimpleJob simpleJob = simpleJobFactory.apply(jobCode);
        simpleJob.run();
    }
}
