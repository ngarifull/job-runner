package com.ngariful.service;

public interface SimpleJobRunner {
    void start(String messageBody);
}
